<?php

namespace App\Http\Livewire;

use App\Models\Requests;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CreateReservation extends Component
{
    public $datePicker;
    public $hours;
    public $minutes;
    public $ampm;
    public $location;
    public $user_id;
    public $whatsapp;
    public $phoneNumber;
    public $game;
    public $platform;


    public function render()
    {
        return view('livewire.create-reservation');
    }

    public function submit()
    {

        $this->user_id = Auth::user();
        $this->validate([
            'platform' => ['required', 'in:ps5,ps4,ps3,xbox,pc'],
            'game' => 'required|in:fifa-22,fifa-21,fifa-20,fifa-19,pes-2022,pes-2021,pes-2020,pes-2019,mortal-kombat',
            'datePicker' => 'required|date_format:Y-m-d',
            'whatsapp' => 'required',
            'phoneNumber' => 'required',
            'hours' => 'between:1,9',
            'minutes' => 'in:0,30',
            'ampm' => 'in:am,pm',
            'location' => 'in:Giza,Helipolis,Zayed,Downtown,5th-Settlement,Nasr-City',
        ]);


        $reservation = new Requests;
        $reservation->platform = $this->platform;
        $reservation->game = $this->game;
        $reservation->date = $this->datePicker;
        $reservation->hours = $this->hours;
        $reservation->minutes = $this->minutes;
        $reservation->location = $this->location;
        $reservation->ampm = $this->ampm;
        $reservation->whatsapp = $this->whatsapp;
        $reservation->phoneNumber = $this->phoneNumber;
        $reservation->user_id = $this->user_id->id;
        $reservation->type = 'single';
        $match = $reservation->platform . $reservation->game . $reservation->date . $reservation->hours . $reservation->minutes . $reservation->location . $reservation->ampm;
        $reservation->match = $match;


        $reservation->save();

        return back()
            ->with('success_message', __('Request created successfully'));

    }
}
