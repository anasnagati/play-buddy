<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Requests;
use Illuminate\Http\Request;

class RequestesControllerApi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Requests::all();
        return response()->json($comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'platform' => 'required|in:ps5,ps4,ps3,xbox,pc',
            'game' => 'required|in:fifa-22,fifa-21,fifa-20,fifa-19,pes-2022,pes-2021,pes-2020,pes-2019,mortal-kombat',
            'datePicker' => 'required|date',
            'hours' => 'between:1,9',
            'minutes' => 'in:0,30',
            'ampm' => 'in:am,pm',
            'location' => 'in:giza,Helipolis,Zayed,Downtown,5th-Settlement,Nasr-City',
            'user_id' => 'unique:requests,deleted_at,NULL'
        ], [
            'user_id.unique' => 'one request por favor'
        ]);

        $request_details = new Requests;

        $request_details->platform = $request->platform;
        $request_details->game = $request->game;
        $request_details->date = $request->datePicker;
        $request_details->hours = $request->hours;
        $request_details->minutes = $request->minutes;
        $request_details->location = $request->location;
        $request_details->ampm = $request->ampm;
        $request_details->whatsapp = $request->whatsapp;
        $request_details->phoneNumber = $request->phoneNumber;
        $request_details->user_id = $request->user_id;

        $request_details->save();

        return response()->json($request_details);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Requests::findOrFail($id);
        return response()->json($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Requests::findOrFail($id);

        $request->validate([
            'name' => 'required|max:255',
            'text' => 'required'
        ]);

        $comment->name = $request->get('name');
        $comment->text = $request->get('text');

        $comment->save();

        return response()->json($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Requests::findOrFail($id);
        $comment->delete();

        return response()->json($comment::all());
    }
}
