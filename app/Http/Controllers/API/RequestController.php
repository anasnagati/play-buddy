<?php



namespace App\Http\Controllers\API;



use App\Models\Requests;
use Illuminate\Http\Request;

use App\Http\Controllers\API\BaseController as BaseController;

use App\Models\Product;

use Illuminate\Support\Facades\Validator;

use App\Http\Resources\RequestResource;



class RequestController extends BaseController

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $requests = Requests::all();



        return $this->sendResponse(RequestResource::collection($requests), 'Products retrieved successfully.');

    }

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $input = $request->all();



        $validator = Validator::make($input, [

            'name' => 'required',

            'detail' => 'required'

        ]);



        if($validator->fails()){

            return $this->sendError('Validation Error.', $validator->errors());

        }



        $request = Requests::create($input);



        return $this->sendResponse(new RequestResource($request), 'Product created successfully.');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        $request = Requests::find($id);



        if (is_null($request)) {

            return $this->sendError('Product not found.');

        }



        return $this->sendResponse(new RequestResource($request), 'Product retrieved successfully.');

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Requests $requests)

    {

        $input = $request->all();

        $validator = Validator::make($input, [

            'name' => 'required',

            'detail' => 'required'

        ]);



        if($validator->fails()){

            return $this->sendError('Validation Error.', $validator->errors());

        }



        $requests->name = $input['name'];

        $requests->detail = $input['detail'];

        $requests->save();



        return $this->sendResponse(new RequestResource($requests), 'Product updated successfully.');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy(Requests $requests)

    {

        $requests->delete();



        return $this->sendResponse([], 'Product deleted successfully.');

    }

}
