<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReservationRequests;
use App\Models\Requests;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RequestsController extends Controller
{
    public function allRequests()
    {
        $number_of_matches = 1;
        $currnet_user_request = Requests::where('user_id', '=', Auth::user()->id)->first()->match;
        $matches = Requests::where('match' , '=', $currnet_user_request)->where('user_id', '!=', Auth::user()->id)->get();
        return view('dashboard', [
            'matches' => $matches,
            'currnet_user_request' => $currnet_user_request,
        ]);
    }

    public function showRandom()
    {
        return view('createRandom');
    }

    public function createRandom(Request $request)
    {
        $request->validate([
            'datePicker' => 'required|date',
            'hours' => 'between:1,9',
            'minutes' => 'in:0,30',
            'ampm' => 'in:am,pm',
            'location' => 'in:giza,Helipolis,Zayed,Downtown,5th-Settlement,Nasr-City',
            'user_id' => 'unique:requests,user_id'
        ], [
            'user_id.unique' => 'one request por favor'
        ]);

        $request_details = new Requests;
        $games = array("fifa-22","fifa-21","fifa-20","fifa-19","pes-2022", "pes-2021", "pes-2020", "pes-2019", "mortal-kombat");
        $platform = array("ps4", "ps5", "xbox", "pc", "ps3");

        $request_details->game = $games[array_rand($games)];
        $request_details->platform = $platform[array_rand($platform)];
        $request_details->date = $request->datePicker;
        $request_details->hours = $request->hours;
        $request_details->minutes = $request->minutes;
        $request_details->location = $request->location;
        $request_details->ampm = $request->ampm;
        $request_details->whatsapp = $request->whatsapp;
        $request_details->phoneNumber = $request->phoneNumber;
        $request_details->user_id = $request->user_id;

        $request_details->save();
        return redirect()->back();
    }
    public function createRequest()
    {
        return view('createRequest');
    }

    public function createRequestPost(Request $request)
    {
        //TODO validation for Platforms should be dynamic
        //TODO validation for Games should be dynamic
        //TODO validation for locations should be dynamic
        $request->validate([
            'platform' => 'required|in:ps5,ps4,ps3,xbox,pc',
            'game' => 'required|in:fifa-22,fifa-21,fifa-20,fifa-19,pes-2022,pes-2021,pes-2020,pes-2019,mortal-kombat',
            'datePicker' => 'required|date',
            'whatsapp' => 'required',
            'phoneNumber' => 'required',
            'hours' => 'between:1,9',
            'minutes' => 'in:0,30',
            'ampm' => 'in:am,pm',
            'location' => 'in:giza,Helipolis,Zayed,Downtown,5th-Settlement,Nasr-City',
            'user_id' => 'unique:requests,user_id'
        ], [
            'user_id.unique' => 'one request por favor'
        ]);

        $request_details = new Requests;

        $request_details->platform = $request->platform;
        $request_details->game = $request->game;
        $request_details->date = $request->datePicker;
        $request_details->hours = $request->hours;
        $request_details->minutes = $request->minutes;
        $request_details->location = $request->location;
        $request_details->ampm = $request->ampm;
        $request_details->whatsapp = $request->whatsapp;
        $request_details->phoneNumber = $request->phoneNumber;
        $request_details->user_id = $request->user_id;
        $match = $request_details->platform . $request_details->game . $request_details->date . $request_details->hours . $request_details->minutes . $request_details->location . $request_details->ampm;
        $request_details->match = $match;

        $request_details->save();
//        dd($match);
        return redirect()->back();
    }

    public function deleteResult($request)
    {
        Requests::where('id', '=', $request)->delete();

        return redirect('/');
    }

    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }
}
