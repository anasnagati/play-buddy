<?php



namespace App\Http\Resources;



use Illuminate\Http\Resources\Json\JsonResource;



class RequestResource extends JsonResource

{

    /**

     * Transform the resource into an array.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable

     */

    public function toArray($request)

    {

        return [

            'id' => $this->id,

            'game' => $this->game,

            'platform' => $this->platform,

            'location' => $this->location,

            'date' => $this->date,

            'hours' => $this->hours,

            'minutes' => $this->minutes,

            'ampm' => $this->ampm,

            'whatsapp' => $this->whatsapp,

            'phoneNumber' => $this->phoneNumber,

            'created_at' => $this->created_at->format('d/m/Y'),

            'updated_at' => $this->updated_at->format('d/m/Y'),

        ];

    }

}
