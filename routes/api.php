<?php

use App\Http\Controllers\API\RequestController;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\RequestesControllerApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('requests', RequestesControllerApi::class);
Route::post('register', [RegisterController::class, 'register']);

Route::post('login', [RegisterController::class, 'login']);


Route::middleware('auth:api')->group( function () {

    Route::resource('requests', RequestController::class);

});
