<div class="mt-5">
    <div class="shadow bg-white text-center w-8/12 mx-auto rounded-lg py-5 px-6 mb-4 text-base mb-3">
        <div class="pt-8">
            {{ __('We couldn\'t find a match for you, yet') }}
        </div>
        <div class=" w-1/12 mx-auto w-auto">
            <div class="mx-auto">
                <div class="pt-8 w-2/12 mx-auto">
                    {!! file_get_contents('icons/sad.svg') !!}
                </div>
            </div>
        </div>
        <a class="bg-teal-500 font-bold text-white shadow p-4 rounded block w-4/12 mx-auto mt-5" href="#">
            {{ __('Make another request ') }}
        </a>
        <div>
            {{ __('or wait for people to match with you') }}
        </div>
    </div>
</div>
