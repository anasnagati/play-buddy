@php
    use App\Models\Requests;
    use Illuminate\Support\Facades\Auth;
    $currnet_user_request = Requests::where('user_id', '=', Auth::user()->id)->first()->match;
    $matches = Requests::where('match' , '=', $currnet_user_request)->where('user_id', '!=', Auth::user()->id)->get();
@endphp

<div class="mt-5">
    <div class="shadow bg-white text-center w-8/12 mx-auto rounded-lg py-5 px-6 mb-4 text-base mb-3">
        <div class="pt-8">
            {{ __('We found a match for you') }}
        </div>
        <div class=" w-1/12 mx-auto w-auto">
            <div class="mx-auto">
                <div class="pt-8 w-2/12 mx-auto">
                    {!! file_get_contents('icons/match.svg') !!}
                </div>
            </div>
        </div>
        <a target="_blank" href="https://api.whatsapp.com/send/?phone={{ $matches[0]->whatsapp }}"
           class="bg-teal-500 font-bold text-white shadow p-4 rounded block w-4/12 mx-auto mt-5" >
            {{ __('Get The Party Started') }}
        </a>
        <p>
            {{ __('Contact your match on whatsapp') }}
        </p>
    </div>
</div>
