
<div
    class="shadow bg-white py-4 text-center rounded-lg my-1 px-1 w-full overflow-hidden sm:my-1 sm:px-1 md:my-2 md:px-2 lg:my-2 lg:px-2 lg:w-1/4 xl:my-3 xl:px-3 xl:w-1/4">
    <div class="pt-8">
        {{ __('We found a match for you') }}
    </div>
    <div class=" w-1/12 mx-auto w-auto">
        <div class="mx-auto">
            <div class="pt-8 w-2/12 mx-auto">
                {!! file_get_contents('icons/match.svg') !!}
            </div>
        </div>
    </div>
    <a target="_blank" href="https://api.whatsapp.com/send/?phone={{ $slot }}"
       class="bg-teal-500 font-bold text-white shadow p-4 rounded block mx-auto mt-5" >
        {{ __('Get The Party Started') }}
    </a>
    <p>
        {{ __('Contact your match on whatsapp') }}
    </p>
</div>


