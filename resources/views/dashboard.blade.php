<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('All Reservations') }}
        </h2>
    </x-slot>

    @if(count($matches) == 0)
        <x-zero-match />
    @elseif(count($matches) == 1)
        <x-single-match/>
    @else
        <div class="mx-auto w-full">
            <div class="flex flex-wrap gap-2 justify-center  -mx-1 overflow-hidden sm:-mx-1 md:-mx-2 lg:-mx-2 xl:-mx-3">
                @foreach($matches as $single_match)
                    <x-multiple-matches> {{ $single_match->whatsapp }} </x-multiple-matches>
                @endforeach
            </div>
        </div>
    @endif
</x-app-layout>
