<div>
    <div class="flex min-h-full flex-col justify-center py-12 sm:px-6 lg:px-8">
        <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
            <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
                <div class="mx-auto flex justify-center">
                    <x-application-logo class="block h-20 fill-current text-gray-600"/>
                </div>
                <form class="space-y-6" method="POST"  wire:submit.prevent>
                    @csrf
                    <div>
                        <label for="platform" class="block text-sm font-medium text-gray-700">
                            Platform </label>
                        <div class="mt-1">
                            <select id="platform" wire:model="platform"
                                    class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                                <option></option>
                                <option value="ps4">PS4</option>
                                <option value="ps5">PS5</option>
                                <option value="xbox">Xbox</option>
                                <option value="pc">PC</option>
                                <option value="ps3">PS3</option>
                            </select>
                            <div>
                            </div>
                            @error('platform') <span class="error">{{ $message }}</span> @enderror
                        </div>

                    </div>

                    <div>
                        <label for="game" class="block text-sm font-medium text-gray-700">
                            Game </label>
                        <div class="mt-1">
                            <select id="game" wire:model="game"
                                    class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                                <option ></option>
                                <option value="fifa-22">Fifa 22</option>
                                <option value="fifa-21">Fifa 21</option>
                                <option value="fifa-20">Fifa 20</option>
                                <option value="fifa-19">Fifa 19</option>
                                <option value="pes-2022">Pes 2022 (We Football)</option>
                                <option value="pes-2021">Pes 2021</option>
                                <option value="pes-2020">Pes 2020</option>
                                <option value="pes-2019">Pes 2019</option>
                                <option value="mortal-kombat">Mortal Kombat</option>
                            </select>
                            @error('game') <span class="error">{{ $message }}</span> @enderror

                        </div>
                        <div class="mt-8">
                            <label for="phoneNumber" class="block text-sm font-medium text-gray-700"> Phone
                                Number </label>
                            <div class="mt-1">
                                <input type="text" value="{{ old('phoneNumber') }}" wire:model="phoneNumber" id="phoneNumber"
                                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                                @error('phoneNumber') <span class="error">{{ $message }}</span> @enderror

                            </div>
                        </div>
                        <div class="mt-8">
                            <label for="whatsapp" class="block text-sm font-medium text-gray-700"> Whatsapp
                                Number </label>
                            <div class="mt-1">
                                <input type="text" value="{{ old('whatsapp') }}" wire:model="whatsapp" id="whatsapp"
                                       class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                                @error('whatsapp') <span class="error">{{ $message }}</span> @enderror

                            </div>
                        </div>

                        <div class="mt-8">
                            <label for="location" class="block text-sm font-medium text-gray-700">
                                Location </label>
                            <div class="mt-1">
                                <select id="location" wire:model="location"
                                        class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                                    <option></option>
                                    <option>Helipolis</option>
                                    <option>Nasr-City</option>
                                    <option>5th-Settlement</option>
                                    <option>Giza</option>
                                    <option>Zayed</option>
                                    <option>Downtown</option>
                                </select>
                                @error('location') <span class="error">{{ $message }}</span> @enderror

                            </div>
                            <div class="mt-8">
                                <div class="relative">
                                    <div
                                        class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                        <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                                             viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
                                                  clip-rule="evenodd"></path>
                                        </svg>
                                    </div>
                                    <input wire:model="datePicker" value="{{ old('datePicker') }}" type="date"
                                           class="border-gray-300 sm:text-sm rounded-lg block w-full pl-10 p-2.5  datepicker-input"
                                           placeholder="Select date">
                                    @error('datePicker') <span class="error">{{ $message }}</span> @enderror

                                </div>

                                <div class="mt-8">
                                    <div class="mt-2 p-2 bg-white rounded-lg ">
                                        <div class="flex">
                                            <select wire:model="hours"
                                                    class="bg-transparent border-gray-300 rounded text-xl appearance-none outline-none">
                                                <option></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">10</option>
                                                <option value="12">12</option>
                                            </select>
                                            <span class="text-xl mr-3"> </span>
                                            <select wire:model="minutes"
                                                    class="bg-transparent border-gray-300 rounded  text-xl appearance-none outline-none mr-4">
                                                <option></option>
                                                <option value="0">00</option>
                                                <option value="30">30</option>
                                            </select>
                                            <select wire:model="ampm"
                                                    class="bg-transparent text-xl border-gray-300 rounded  appearance-none outline-none">
                                                <option></option>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div>
                        <button wire:click="submit"
                                class="flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                            {{ __('Create Your Reservation') }}
                        </button>
                    </div>
                    <x-success-message/>

                </form>
            </div>
        </div>
    </div>

</div>
